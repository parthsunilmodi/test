export const getDynamicArrayFromLevels = (level, multiply) => {
  if (multiply) {
    return Array.from({ length: level * level }, (_, idx) => ++idx)
  }
  return Array.from({ length: level }, (_, idx) => ++idx)
};
