export const getAnswers = (array) => {
  const length = array == null ? 0 : array.length;
  let selectedCount = Math.ceil(length / 2);
  if (!length || selectedCount < 1) {
    return []
  }
  selectedCount = selectedCount > length ? length : selectedCount;
  let index = -1;
  const result = array.slice();
  while (++index < selectedCount) {
    const rand = index + Math.floor(Math.random() * (length - index));
    const value = result[rand];
    result[rand] = result[index];
    result[index] = value
  }
  return result.slice(0, selectedCount)
};
