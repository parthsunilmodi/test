import React, { useEffect, useRef, useState } from 'react';
import { difference } from 'lodash';
import { getAnswers } from './utils/getAnswers';
import { getDynamicArrayFromLevels } from "./utils/dynamicArrayFromLevels";
import TilesView from './components/TilesView';
import Result from './components/Result';
import './App.scss';

function App() {
  const [level, setLevel] = useState(1);
  const [lives, setLives] = useState(5);
  const [selectedTiles, setSelectedTiles] = useState([]);
  const [showAnswers, toggleShowAnswers] = useState(true);
  const [modalDetails, setModalDetails] = useState(null);

  const allTiles = useRef(getDynamicArrayFromLevels(level + 2, true));
  const answersRef = useRef(getAnswers(allTiles.current));

  useEffect(() => {
    if (showAnswers) {
      setTimeout(() => {
        toggleShowAnswers(false);
      }, 2000);
    }
  }, [showAnswers]);

   useEffect(() => {
    if(lives === 0) {
      setModalDetails({ status: 'warning', title: 'Game Over..!!' });
    }
  },[lives, level]);

  useEffect(() => {
    if (!difference(answersRef.current, selectedTiles).length) {
      setModalDetails({ status: 'success', title: 'Congratulations!! You Crossed the Level' });
    }
  }, [selectedTiles]);

  const onOkay = () => {
    if (!lives) {
      setLevel(1);
      setLives(5);
      allTiles.current = getDynamicArrayFromLevels(3, true);
      answersRef.current = getAnswers(allTiles.current);
      setModalDetails(null);
      toggleShowAnswers(true);
      setSelectedTiles([]);
    } else {
      setLevel(level + 1);
      allTiles.current = getDynamicArrayFromLevels(level + 3, true);
      answersRef.current = getAnswers(allTiles.current);
      setModalDetails(null);
      toggleShowAnswers(true);
      setSelectedTiles([]);
    }
  };

  return (
    <div className="App">
      {
        modalDetails && (
          <Result title={modalDetails.title} status={modalDetails.status} onOkay={onOkay} />
        )
      }
      <div className="info-wrapper">
        <div className="info">Lives: <span>{lives}</span></div>
        <div className="info">Level: <span>{level}</span></div>
      </div>
      <TilesView
        level={level}
        lives={lives}
        setLives={setLives}
        allTiles={allTiles.current}
        answers={answersRef.current}
        showAnswers={showAnswers}
        selectedTiles={selectedTiles}
        setSelectedTiles={setSelectedTiles}
      />
    </div>
  );
}

export default App;
