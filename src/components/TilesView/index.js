import React, {useEffect, useState} from 'react';
import { getDynamicArrayFromLevels } from "../../utils/dynamicArrayFromLevels";
import './tilesView.scss';

const TilesView = (props) => {
  const { level, answers, showAnswers, lives, setLives, selectedTiles, setSelectedTiles } = props;
  const [color, setColor] = useState('');

  useEffect(() => {
    setColor(Math.random().toString(16).substr(-6));
  }, [level]);

  const onTilesClick = (id) => () => {
    if (answers.includes(id)) {
      setSelectedTiles([...selectedTiles, id]);
    } else {
      setLives(lives - 1);
    }
  };

  const isTilesSelected = (id) => {
    return (answers.includes(id) && showAnswers) || selectedTiles.includes(id);
  };

  const rowsCols = getDynamicArrayFromLevels(level + 2);

  return (
    <div className="wrapper">
      {
        rowsCols.map((row, rowIndex) => {
          return (
            <div className="row">
              {
                rowsCols.map((col) => {
                  const id = (rowIndex * (level + 2)) + col;
                  return (
                    <div
                      className="column"
                      onClick={onTilesClick(id)}
                      style={{ background: isTilesSelected(id) ? `#${color}` : '#fff', border: `1px solid #000` }}
                    />
                  )
                })
              }
            </div>
          )
        })
      }
    </div>
  )
};

export default TilesView;
