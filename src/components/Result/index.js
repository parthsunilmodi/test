import React from 'react';
import { Result as AntdResult, Button, Modal } from 'antd';

const Result = ({ status, title, onOkay }) => {
  return (
    <Modal visible footer={null}>
      <AntdResult
        status={status || 'success'}
        title={title || ''}
        extra={[
          <Button type="primary" key="okay" onClick={onOkay}>
            Okay
          </Button>
        ]}
      />
    </Modal>
  )
};

export default Result;
